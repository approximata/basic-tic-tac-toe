'use strict';

const playState = [
    [ 0,0,0 ],
    [ 0,0,0 ],
    [ 0,0,0 ]
];

const renderState = (state) => {
    const playGround = document.querySelector('.playground'); 
    state.forEach((line, xIndex) => {
        const lineElement = document.createElement('div');
        lineElement.classList.add('line');
        playGround.appendChild(lineElement);
        line.forEach((node, yIndex) => {
            const nodeElement = document.createElement('div');
            nodeElement.classList.add('node');
            nodeElement.dataset.postition = `${xIndex}-${yIndex}`;
            const nodeValue = document.createElement('div');
            nodeValue.classList.add('node-value');
            nodeValue.appendChild(document.createTextNode(node))
            nodeElement.appendChild(nodeValue);
            lineElement.appendChild(nodeElement);
            nodeElement.addEventListener('click', () => draw(nodeElement));
        });
    });
};

const clearPlay = () => {
    const playGround = document.querySelector('.playground'); 
    playGround.innerHTML = '';
};

const validateState = (state) => {

    const xCrossState = [];
    state.forEach((line, lineIndex) => { line.forEach((node, nodeIndex) => {
        if(lineIndex === nodeIndex) {xCrossState.push(node)}
        })
    });

    const yCrossState = [];
    state.forEach((line, lineIndex) => {
        line.forEach((node, nodeIndex) => {
            if (nodeIndex === (line.length-1)-lineIndex) { yCrossState.push(node) }
        })
    });

    const transformedState = [[],[],[]];
    state.forEach(line => {
        line.forEach((node, nodeIndex) => {
            transformedState[nodeIndex].push(node);
        })
    });

    console.table(xCrossState);
    console.table(yCrossState);
    console.table(transformedState);

    const winnerLines = [];

    state.forEach(line => winnerLines.push(line));
    transformedState.forEach(line => winnerLines.push(line));
    winnerLines.push(xCrossState);
    winnerLines.push(yCrossState);

    const isLineWin = line => {
        let lineValues = '';
        line.forEach(node => { lineValues += node })
        return lineValues === 'xxx' || lineValues === 'ooo'
    }

     if (winnerLines.some(line => isLineWin(line))) {
        alert(`${player} won`);
     }
}

let player = 'x';

const draw = (node) => {
    const [x, y] = node.dataset.postition.split('-');
    player = player === 'x' ? 'o' : 'x';
    playState[x][y] = player;
    clearPlay();
    renderState(playState);
    validateState(playState);
};



renderState(playState);
